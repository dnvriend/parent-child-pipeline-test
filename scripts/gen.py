#!/usr/local/bin/python
import os
import yaml
import argparse

from typing import List, Optional


class Job:
    LINTING = 'Linting'
    VALIDATE_TERRAFORM = 'Validate Terraform'
    TERRAFORM_PLAN = 'Terraform Plan'
    TERRAFORM_PLAN_COMMON = 'Terraform Plan Common'
    APPROVAL = 'Approval'
    APPROVAL_COMMON = 'Approval Common'
    DEPLOY_CHANGES = 'Deploy changes'
    DEPLOY_COMMON = 'Deploy Common'


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def load_yaml(filename: str) -> dict:
    "returns a yaml file as a dict"
    return yaml.load(open(filename, "r"), Loader=yaml.Loader)


def write_yaml(file: str, data: dict):
    "writes a dict to a yaml file to a location"
    yaml.dump(data, open(file, 'w'), Dumper=yaml.Dumper)


def get_workload_environments(environments_dict: dict) -> List[str]:
    "returns all the environments of a workload"
    return [x.get('name') for x in environments_dict['environments']]


def get_project_environments() -> List[str]:
    "returns the project environments that are configured in a gitlab project"
    xs = [(x.lower(), os.getenv('ENABLE_' + x)) for x in ['DV', 'ST', 'AT', 'PR'] if os.getenv('ENABLE_' + x) == 'true']
    return list(dict(xs).keys())


def get_project_env_vs_workload_env(workload_environments: List[str], project_environments: List[str]) -> dict:
    "returns a dict with all the environments per environment key"
    project_env_vs_workload_env = dict({'dv': set(), 'st': set(), 'at': set(), 'pr': set()})
    for env in project_environments:
        for wl_env in workload_environments:
            if env in wl_env:
                project_env_vs_workload_env.get(env).add(wl_env)

    return project_env_vs_workload_env


def has_common_layer(env: str, project_env_vs_workload_env: dict) -> bool:
    return len(get_common_layer(env, project_env_vs_workload_env)) != 0


def get_common_layer(env: str, project_env_vs_workload_env: dict) -> List[str]:
    "for an environment, return the common layer"
    return [stage for stage in project_env_vs_workload_env.get(env) if 'common' in stage]


def has_silos(env: str, project_env_vs_workload_env: dict) -> bool:
    return len(get_silos(env, project_env_vs_workload_env)) != 0


def get_silos(env: str, project_env_vs_workload_env: dict) -> List[str]:
    "for an environment, it returns only the silos"
    return [stage for stage in project_env_vs_workload_env.get(env) if 'common' not in stage]


def determine_first(xs: List[str]) -> Optional[str]:
    "returns the head of the list"
    if len(xs) == 0:
        return None
    else:
        return xs[0]


def has_first(xs: List[str]) -> bool:
    "determines whether or not project environments has a first"
    return not (len(xs) == 1 and 'pr' in xs)


def add_matrix(key: str, doc: dict, stages: List[str]):
    "add parallel.matrix to a job"
    if len(stages) != 0:
        print(f'{bcolors.OKGREEN}Add matrix to {key} with {stages}{bcolors.ENDC}')
        y = {'parallel': {'matrix': [{'STAGE': stages}]}}
        doc[key] = {**doc[key], **y}
    else:
        print(f'{bcolors.FAIL}No matrix for [{key}]{bcolors.ENDC}')
        add_disable(key, doc)


def add_disable(key: str, doc: dict):
    "disables a job by prepending a job with a .dot"
    print(f'{bcolors.FAIL}Disabling [{key}]{bcolors.ENDC}')
    doc['.' + key] = doc.pop(key)


def gen_pre(template_dict: dict, project_environments: List[str], project_env_vs_workload_env_dict: dict):
    "generates the pre pipeline"
    first = determine_first(project_environments)
    if has_first(project_environments):
        gen_pipeline(template_dict, first, project_env_vs_workload_env_dict)
    else:
        if has_silos(first, project_env_vs_workload_env_dict):
            add_matrix(Job.TERRAFORM_PLAN, template_dict, get_silos(first, project_env_vs_workload_env_dict))
        if has_common_layer(first, project_env_vs_workload_env_dict):
            add_matrix(Job.TERRAFORM_PLAN_COMMON, template_dict, get_silos(first, project_env_vs_workload_env_dict))

        add_disable(Job.APPROVAL, template_dict)
        add_disable(Job.APPROVAL_COMMON, template_dict)
        add_disable(Job.DEPLOY_CHANGES, template_dict)
        add_disable(Job.DEPLOY_COMMON, template_dict)


def gen_pipeline(template_dict: dict, env: str, project_env_vs_workload_env: dict):
    "generates a generic pipeline"
    if has_silos(env, project_env_vs_workload_env):
        add_matrix(Job.TERRAFORM_PLAN, template_dict, get_silos(env, project_env_vs_workload_env))
        add_matrix(Job.DEPLOY_CHANGES, template_dict, get_silos(env, project_env_vs_workload_env))
    else:
        add_disable(Job.TERRAFORM_PLAN, template_dict)
        add_disable(Job.APPROVAL, template_dict)
        add_disable(Job.DEPLOY_CHANGES, template_dict)

    if has_common_layer(env, project_env_vs_workload_env):
        add_matrix(Job.TERRAFORM_PLAN_COMMON, template_dict, get_common_layer(env, project_env_vs_workload_env))
        add_matrix(Job.DEPLOY_COMMON, template_dict, get_common_layer(env, project_env_vs_workload_env))
    else:
        add_disable(Job.TERRAFORM_PLAN_COMMON, template_dict)
        add_disable(Job.APPROVAL_COMMON, template_dict)
        add_disable(Job.DEPLOY_COMMON, template_dict)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate pipeline.')
    parser.add_argument('-p', '--pipeline',
                        metavar='pipeline',
                        type=str,
                        nargs=1,
                        choices=['pre', 'st', 'at', 'pr'],
                        required=True,
                        help='the name of the child pipeline to generate eg. pre')
    parser.add_argument('-e', '--environment',
                        metavar='environment',
                        type=str,
                        nargs=1,
                        required=True,
                        help='the full path to environments.yml eg. /environments.yml')
    parser.add_argument('-t', '--to',
                        metavar='to',
                        type=str,
                        nargs=1,
                        required=True,
                        help='the path to write to eg. /output')
    parser.add_argument('-d', '--dir',
                        metavar='dir',
                        type=str,
                        nargs=1,
                        required=True,
                        help='the path to the template dir eg. /templates')

    args = parser.parse_args()
    stage_name = args.pipeline[0]
    output_dir = args.to[0]
    templates_dir = args.dir[0]
    path_to_environments_yml = args.environment[0]

    template_filename = f'{templates_dir}/{stage_name}.yml'
    to_filename = f'{output_dir}/{stage_name}-gitlab-ci.yml'

    template_dict = load_yaml(template_filename)
    environments_dict = load_yaml(path_to_environments_yml)

    project_env_vs_workload_env_dict = get_project_env_vs_workload_env(get_workload_environments(environments_dict), get_project_environments())

    if stage_name == 'pre':
        gen_pre(template_dict, get_project_environments(), project_env_vs_workload_env_dict)
    else:
        gen_pipeline(template_dict, stage_name, project_env_vs_workload_env_dict)

    write_yaml(to_filename, template_dict)
    print(f'{to_filename} written')
